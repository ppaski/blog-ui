import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
import { TOKEN_NAME } from './auth.service';


@Injectable()
export class JWTInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>>
    {
        let jwtToken = sessionStorage.getItem(TOKEN_NAME);
        if (jwtToken)
        {
            req = req.clone({
                headers: req.headers.set("Authorization", " Bearer " + jwtToken)
            });
        }

        return next.handle(req);
    }
}