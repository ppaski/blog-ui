import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../Models/Post';
import { PostsService } from '../posts.service';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy  {
  private readonly onDestroy = new Subject<void>();
  public posts: Post[];
  constructor(
    private httpClient: HttpClient) { }
  ngOnInit() {

    this.getAll();
  }
  ngOnDestroy() {
    this.onDestroy.next();
  }

  private getAll(): void {
    this.httpClient.get("http://127.0.0.1:8000/api/posts").subscribe((res: any) => {
      let posts = res.data as Post[];

      this.posts = posts
      }
    )
  }
}
