import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router' ;
import { Validators } from '@angular/forms';
import { UserLoginModel } from '../Models/User';
import { AuthService } from '../auth.service';
import { Post, PostSaveModel } from '../Models/Post';
import { PostsService } from '../posts.service';


@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  constructor(private postsService: PostsService) { }

  ngOnInit() {
  }
  public imageSrc: string = '';
  public postForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    content: new FormControl('', [Validators.required])
  });

  get title(): any {
    return this.postForm.get('title');
  }

  get content(): any {
    return this.postForm.get('content');
  }

  public convertImageToBase64(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    let reader = e.target;
    this.imageSrc = reader.result;
    console.log(this.imageSrc)
  }

  public onSubmit(): void {
    let post = {
      title: this.postForm.get('title').value,
      content: this.postForm.get("content").value,
      image: this.imageSrc
    } as PostSaveModel;

    this.postsService.create(post);
  }

  public isAnyImageSelected(): boolean {
    return !!this.imageSrc;
  }
}
