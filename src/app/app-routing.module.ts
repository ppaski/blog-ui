import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './authGuard';
import { CreatePostComponent } from './create-post/create-post.component';
import { SinglePostComponent } from './single-post/single-post.component';
import { UsersComponent } from './users/users.component';
import { CreateUserComponent } from './create-user/create-user.component';

const routes: Routes = [    
  {
  path: 'home',
  component: HomeComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'post/create',
    component: CreatePostComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'users/create',
    component: CreateUserComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'posts/:id',
    component: SinglePostComponent
  },
  {
    path: 'users',
    component: UsersComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
},
{
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
