import { Component, OnInit } from '@angular/core';
import { User } from '../Models/User';
import { faTrash, faUserEdit, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public faTrash = faTrash;
  public faUserEdit = faUserEdit;
  public faUserPlus = faUserPlus;
  public users: User[]
  constructor(
    private notifierService: NotifierService,
    private httpClient: HttpClient,
    private router: Router) { }

  ngOnInit() {
    this.getUsers();
  }

  public getUsers(): void {
    this.httpClient.get("http://127.0.0.1:8000/api/users").subscribe((res: any) => {
      let users = res.data as User[];

      this.users = users;
      })
  }

  public userRemove(id: number, username: string): void {
      this.httpClient.delete(`http://127.0.0.1:8000/api/users/${id}`).subscribe((_: any) => {
         this.notifierService.notify("success", `Użytkownik ${username} został usunięty`)
         this.getUsers();
      })
  }

  public userEdit(id: number ): void {
    console.log(`${id}  userEdit`)
  }

  public createUser(): void {
    this.router.navigate(["./users/create"]);
  }
}
