import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router' ;
import { Validators } from '@angular/forms';
import { UserLoginModel } from '../Models/User';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService) { }

  userForm = new FormGroup({
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    email: new FormControl('', [Validators.required, Validators.pattern(".+\\@.+\\...")])
  });

  get password(): any {
    return this.userForm.get('password');
  }

  get email(): any {
    return this.userForm.get('email');
  }

  ngOnInit() {
  }

  public onSubmit(): void {
    let user = {
      email: this.userForm.get('email').value,
      password: this.userForm.get("password").value,
    } as UserLoginModel;

    this.authService.login(user);
  }
}
