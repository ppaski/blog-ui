import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router' ;
import { Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { UserRegisterModel } from '../Models/User';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private notifierService: NotifierService) { }

  userForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    confirmPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
    email: new FormControl('', [Validators.required, Validators.pattern(".+\\@.+\\.[a-z]+")])
  }, this.ensurePasswordAreEqual);

  get username(): any {
    let username = this.userForm.get('username');;
    return this.userForm.get('username');
  }

  get password(): any {
    return this.userForm.get('password');
  }

  get confirmPassword(): any {
    return this.userForm.get('confirmPassword');
  }

  get email(): any {
    return this.userForm.get('email');
  }
  
  ensurePasswordAreEqual()
  {
    if (!this || !this.userForm) {
      return null;
    }
    let password = this.userForm.get("password").value;
    let confirmPassword = this.userForm.get("confirmPassword").value;

    if (password == confirmPassword)
    {
      return null;
    }

    this.notifierService.notify(
      "error",
      "Podane hasłą różnią się"
  );
    return  {valid: false};
  }
  
  ngOnInit() {
  }

  public onSubmit(): void {
    if (!!this.ensurePasswordAreEqual()){
      return;
    }
    let user = {
      name: this.userForm.get("username").value,
      email: this.userForm.get('email').value,
      password: this.userForm.get("password").value,
      c_password: this.userForm.get("confirmPassword").value
    } as UserRegisterModel;

    this.authService.register(user);
  }

}
