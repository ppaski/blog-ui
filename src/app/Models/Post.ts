import { Url } from 'url';

export interface Post {
    id: number;
    title: string;
    content: string;
    author: string;
    created_on: Date;
    image: string;
}

export interface PostSaveModel {
    title: string;
    content: string; 
    image: string;
}