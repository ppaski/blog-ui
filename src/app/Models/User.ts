export interface UserRegisterModel {
    name: string;
    email: string;
    password: string;
    c_password: string;
}

export interface UserLoginModel {
    email: string;
    password: string;
}

export interface RegisterResponse{
    success: boolean;
    message: string;
    data: RegisterResponseData
}

export interface RegisterResponseData {
    token: string;
    name: string;
}

export interface User {
    id: number,
    name: string,
    email: string
}