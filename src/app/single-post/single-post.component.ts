import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../Models/Post';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { faEdit, faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { PostsService } from '../posts.service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})
export class SinglePostComponent implements OnInit {

  constructor(
    private httpClient: HttpClient,
    private route: ActivatedRoute,
    private authService: AuthService,
    private postsService: PostsService,
    private router: Router,
    private notifierService: NotifierService) { }

  public isInEditMode: boolean;
  public faEdit = faEdit;
  public faTrashAlt = faTrashAlt;
  public postModel: Post;
  public footerText: string;
  public isUserAuthorized: boolean
  public postForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    content: new FormControl('', [Validators.required])
  });

  get title(): any {
    return this.postForm.get('title');
  }

  get content(): any {
    return this.postForm.get('content');
  }

  ngOnInit() {
    this.postModel = {
      id: 0,
      title: "",
      author: "",
      content: "",
      image: "",
      created_on: undefined
    } as Post;
    const id = this.route.snapshot.paramMap.get("id");
    this.get(id);
    this.authService.userLoggedId.subscribe(isLoggedIn => this.isUserAuthorized = isLoggedIn)
    this.isInEditMode = false;
  }

  public onSubmit(): void {
    let post = {
      title: this.postForm.get('title').value,
      content: this.postForm.get("content").value,
      id: this.postModel.id
    } as Post;

    this.postsService.update(post);
  }

  public delete(): void {
    this.httpClient.delete(`http://127.0.0.1:8000/api/posts/${this.postModel.id}`).subscribe((_: any) => {
      this.notifierService.notify("success", `Post został usunięty`);
      this.router.navigate(["./"]);
   })
  }

  private get(id: string): void {
    //http://zaiblog.westeurope.cloudapp.azure.com:8000
    this.httpClient.get(`http://127.0.0.1:8000/api/posts/${id}`).subscribe((res: any) => {
      let post = res.data as Post[];

      this.postModel = post[0];
      console.log(this.postModel.content);
      this.setFooterText();
      } 
    )
  }

  private setFooterText(): void {
    this.footerText = `${this.postModel.author}, ${this.postModel.created_on}`
  }

  public edit(): void {
    this.isInEditMode = true;
  }
}
