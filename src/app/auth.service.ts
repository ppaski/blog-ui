import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserRegisterModel, RegisterResponseData, RegisterResponse, UserLoginModel } from './Models/User';
import { HttpResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import { NotifierService } from 'angular-notifier';

export const TOKEN_NAME: string = 'JWT';
export const CURRENT_USERNAME: string = "CURRENT_USERNAME";
export const CURRENT_USERID: string = "CURRENT_USERID";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Accept': 'application/json'
  }),
  observe: 'response'
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private helper = new JwtHelperService();
  public userLoggedId: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public currentUsername: BehaviorSubject<string> = new BehaviorSubject<string>("");

  constructor(
    private httpClient: HttpClient,
    private router : Router,
    private notifierService: NotifierService) {
      this.userLoggedId.next(this.isUserLoggedIn());
      this.currentUsername.next(this.getCurrentUserName());
    }

  public getToken() : string {
      return sessionStorage[TOKEN_NAME];
  }

  public setToken(token: string, username: string, id: string): void{
      sessionStorage.setItem(TOKEN_NAME, token);
      sessionStorage.setItem(CURRENT_USERNAME, username);
      sessionStorage.setItem(CURRENT_USERID, id);
  }

  public removeToken(): void {
      sessionStorage.removeItem(TOKEN_NAME);
      sessionStorage.removeItem(CURRENT_USERNAME);
      sessionStorage.removeItem(CURRENT_USERID);
  }

  public logOut(): void {
    this.removeToken();
    this.userLoggedId.next(true);
    this.currentUsername.next("");
    location.reload();
    this.router.navigate(["./"]);
  }

  public isTokenExpired(): boolean {
      var token = this.getToken();
      if (!token){
          return true;
      }

      return this.helper.isTokenExpired(token);
  }

  public createUser(user: UserRegisterModel): void {
    this.httpClient.post("http://127.0.0.1:8000/api/register", user, {
      observe: 'response'
    })
    .subscribe( (resp: any) => {
      if (resp.status === 200 && resp.statusText === "OK") {
        this.router.navigate(["./users"]);
      }
    })
  }

  public register(user: UserRegisterModel): void {
    this.httpClient.post("http://127.0.0.1:8000/api/register", user, {
      observe: 'response'
    })
    .subscribe( (resp: any) => {
      if (resp.status === 200 && resp.statusText === "OK") {
        const token = resp.body.data.token;
        const username = resp.body.data.username;
        const userId = resp.body.data.userId;

        this.setToken(token, username, userId);
        this.userLoggedId.next(true);
        this.currentUsername.next(username);
        this.router.navigate(["./"]);
      }
    },
    error => {
      this.notifierService.notify(
        "error",
        error.error.message
    );})
  }

  public login(user: UserLoginModel): void {
    this.httpClient.post("http://127.0.0.1:8000/api/login", user, {
      observe: 'response'
    })
    .subscribe( (resp: any) => {
      if (resp.status === 200 && resp.statusText === "OK") {
        const token = resp.body.data.token;
        const username = resp.body.data.username;
        const userId = resp.body.data.userId;

        this.setToken(token, username, userId);
        this.userLoggedId.next(true);
        this.currentUsername.next(username);
        this.router.navigate(["./"]);
      }
    },
    error => {
      this.notifierService.notify(
        "error",
        error.error.message
    );}
    )
  }

  private isUserLoggedIn(): boolean {
    var token = this.getToken();

    return !!token;
  }

  private getCurrentUserName(): string {
    var username = sessionStorage[CURRENT_USERNAME];

    return username;
  }
}
