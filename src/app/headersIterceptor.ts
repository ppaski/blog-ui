import { Injectable } from "../../node_modules/@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "../../node_modules/@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HeadersIterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>>
    {
        req = req.clone({
            headers: req.headers.set("Content-type", "application/json")
            .set("Accept", "application/json")
        });

        return next.handle(req);
    }
}