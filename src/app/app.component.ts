import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'blog';
  public isUserAuthorized: boolean
  public currentUsername: string;

  constructor(private authService: AuthService) {
    this.isUserAuthorized = false;
    this.currentUsername = "";
  }

  ngOnInit(): void {
    this.authService.userLoggedId.subscribe(isLoggedIn => this.isUserAuthorized = isLoggedIn)
    this.authService.currentUsername.subscribe(username => this.currentUsername = username);
  }

  public logOut(): void {
    this.authService.logOut();
  }
}
