import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../Models/Post';
import { Router } from '@angular/router';

const maxTextLengthOnMainPage = 300;

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})

export class PostComponent implements OnInit {

  @Input()
  postModel: Post;

  @Input()
  isOnMainPage: boolean
  public contentText: string;
  public footerText: string;
  public url: string;

  private isPostLong: boolean
  constructor(private router: Router) { }

  ngOnInit() {
    this.setContentText();
    this.setFooterText();
    this.generateLink();
  }

  public showMore(): boolean {
    return this.isOnMainPage && this.postModel.content.length > maxTextLengthOnMainPage
  }

  private setContentText(): void {
    if (!this.isOnMainPage) {
      this.contentText = this.postModel.content;

      return;
    }
    let trimmedString = this.postModel.content.substr(0, maxTextLengthOnMainPage);

    //re-trim if we are in the middle of a word
    if (trimmedString.lastIndexOf(" ") ===-1) {
      this.contentText = trimmedString;
    }
    else {
      this.contentText = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))
    }
  }

  private setFooterText(): void {
    this.footerText = `${this.postModel.author}, ${this.postModel.created_on}`
  }

  private generateLink(): void {
    this.url = `/posts/${this.postModel.id}`
  }

}
