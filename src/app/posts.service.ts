import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post, PostSaveModel } from './Models/Post';
import { map } from "rxjs/operators";
import { of, Observable } from 'rxjs';
import { NotifierService } from 'angular-notifier';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(
    private httpClient: HttpClient,
    private notifierService: NotifierService,
    private router : Router) { }

  public create(post: PostSaveModel): void {
    this.httpClient.post("http://127.0.0.1:8000/api/posts", post, {
      observe: 'response'
    })
    .subscribe( (resp: any) => {
      if (resp.status === 200 && resp.statusText === "OK") {
        this.router.navigate(["/"]);
        this.notifierService.show({
          type: "success",
          message: "Post został utworzony"
      });
      }
    }, 
    error => {
      this.notifierService.notify(
        "error",
        error.error.message
    );})
  }

  public update(post: Post): void {
    this.httpClient.post(`http://127.0.0.1:8000/api/posts/${post.id}`, post, {
      observe: 'response'
    })
    .subscribe( (resp: any) => {
      if (resp.status === 200 && resp.statusText === "OK") {
        this.router.navigate(["/"]);
        this.notifierService.show({
          type: "success",
          message: "Post został zaktualizowany"
      });
      }
    }, 
    error => {
      this.notifierService.notify(
        "error",
        error.error.message
    );})
  }

  public getAll(): Observable<Post[]> {
    this.httpClient.get("http://127.0.0.1:8000/api/posts").subscribe((res: any) => {
      let posts = res.data as Post[];

      return of(posts);
      }
    )

    let emptyArray = [] as Post[];
    return of(emptyArray);
  }
}
